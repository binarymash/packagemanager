﻿namespace BinaryMash.Responses
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    public class Response
    {
        public Response()
        {
            Errors = new List<Error>();
        }

        public Response(List<Error> errors)
        {
            Errors = errors ?? new List<Error>();
        }

        public List<Error> Errors { get; private set; }
    }

    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "This is a generic implementation of the class above")]
    public class Response<T> : Response
    {
        public Response(T data)
        {
            Data = data;
        }

        public Response(List<Error> errors)
            : base(errors)
        {
        }

        public T Data { get; private set; }
    }
}
