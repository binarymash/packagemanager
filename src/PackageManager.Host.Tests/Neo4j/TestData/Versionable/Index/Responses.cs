﻿namespace PackageManager.Host.Tests.Neo4j.TestData.Versionable.Index
{
    using System.IO;
    using System.Reflection;
    using System.Text;

    public static class Responses
    {
        public static Host.Neo4j.Versionable.Index.Response Nominal
        {
            get
            {
                var assembly = typeof(Responses).GetTypeInfo().Assembly;
                var resourceNames = assembly.GetManifestResourceNames();
                var resourceStream = assembly.GetManifestResourceStream("PackageManager.Host.Tests.Neo4j.TestData.Versionable.Index.Response.Nominal.json");

                using (var reader = new StreamReader(resourceStream, Encoding.UTF8))
                {
                    var nominalString = reader.ReadToEnd();
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<Host.Neo4j.Versionable.Index.Response>(nominalString);
                }
            }
        }
    }
}
