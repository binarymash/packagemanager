﻿namespace PackageManager.Host.Tests.Neo4j.TestData.Versionable.Create
{
    public static class Statements
    {
        public static Host.Neo4j.Versionable.Create.RequestStatement Statement1
        {
            get
            {
                return new Host.Neo4j.Versionable.Create.RequestStatement(
                    "Reference1Name",
                    "Referrer2Version",
                    "ReferenceType",
                    "Dependency1Name",
                    "Dependency1Version");
            }
        }

        public static Host.Neo4j.Versionable.Create.RequestStatement Statement2
        {
            get
            {
                return new Host.Neo4j.Versionable.Create.RequestStatement(
                    "Reference2Name",
                    "Referrer2Version",
                    "ReferenceType",
                    "Dependency2Name",
                    "Dependency2Version");
            }
        }
    }
}
