﻿namespace PackageManager.Host.Tests.Neo4j.TestData.Versionable.Create
{
    using System.Collections.Generic;

    public static class Requests
    {
        public static Host.Neo4j.Versionable.Create.Request Nominal
        {
            get
            {
                return new Host.Neo4j.Versionable.Create.Request
                {
                    Statements = new List<Host.Neo4j.Generic.RequestStatement>
                    {
                        Statements.Statement1,
                        Statements.Statement2
                    }
                };
            }
        }
    }
}
