﻿namespace PackageManager.Host.Tests.Neo4j.TestData.Versionable.Create
{
    using PackageManager.Host.Neo4j.Versionable.Create;

    public static class Responses
    {
        public static Response Ok
        {
            get { return new Response(); }
        }
    }
}
