﻿namespace PackageManager.Host.Tests.Features.Versionable
{
    using System.Collections.Generic;
    using System.Net;
    using BinaryMash.Responses;
    using MediatR;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using Shouldly;
    using TestStack.BDDfy;
    using Xunit;
    using Versionable = Host.Features.Versionable;

    public class ApiControllerSpecs
    {
        private readonly Versionable.ApiController _controller;

        private readonly Mock<IMediator> _mediator;

        private Mock<HttpContext> _httpContext;

        private Mock<HttpResponse> _httpResponse;

        private IActionResult _result;

        private Versionable.Index.Model.Query _indexQuery;

        private Versionable.Create.Model.Command _createCommand;

        private object _responseFromMediator;

        public ApiControllerSpecs()
        {
            _mediator = new Mock<IMediator>();

            _httpResponse = new Mock<HttpResponse>();

            _httpContext = new Mock<HttpContext>();
            _httpContext.Setup(hc => hc.Response).Returns(_httpResponse.Object);

            var controllerContext = new ControllerContext();
            controllerContext.HttpContext = _httpContext.Object;

            _controller = new Versionable.ApiController(_mediator.Object);
            _controller.ControllerContext = new ControllerContext();
        }

        [Fact]
        public void ShouldReturnEmptyCommandOnGet()
        {
            this.When(_ => _.WhenWeGetACreateCommand())
                .Then(_ => _.ThenTheHttpStatusCodeIs(HttpStatusCode.OK))
                .And(_ => _.ThenWeAreReturnedAnEmptyCreateCommand())
                .BDDfy();
        }

        [Fact]
        public void ShouldCreateVersionable()
        {
            this.Given(_ => _.GivenAValidCreateCommand())
                .And(_ => _.GivenTheCreateCommandWillSucceed())
                .When(_ => _.WhenWePostTheCreateCommand())
                .Then(_ => _.ThenTheCommandIsSentToTheMediator())
                .And(_ => _.ThenTheHttpStatusCodeIs(HttpStatusCode.Created))
                .BDDfy();
        }

        [Fact]
        public void ShouldReturnVersionablesWhenGetIndexQueryFindsMatches()
        {
            this.Given(_ => _.GivenAnIndexQuery())
                .And(_ => _.GivenThatThereAreVersionablesThatMatchTheQuery())
                .When(_ => _.WhenWeGetTheIndexQuery())
                .Then(_ => _.ThenTheHttpStatusCodeIs(HttpStatusCode.OK))
                .And(_ => _.ThenTheVersionablesAreReturned())
                .BDDfy();
        }

        [Fact]
        public void ShouldReturnEmptyCollectionWhenGetIndexQueryFindsNoMatches()
        {
            this.Given(_ => _.GivenAnIndexQuery())
                .And(_ => _.GivenThatThereAreNoVersionablesThatMatchTheQuery())
                .When(_ => _.WhenWeGetTheIndexQuery())
                .Then(_ => _.ThenTheHttpStatusCodeIs(HttpStatusCode.OK))
                .And(_ => _.ThenAnResponseIsReturnedThatContainsNoVersionablesOrDependencies())
                .BDDfy();
        }

        private void GivenAnIndexQuery()
        {
            _indexQuery = new Versionable.Index.Model.Query();
        }

        private void GivenAValidCreateCommand()
        {
            _createCommand = new Versionable.Create.Model.Command();
        }

        private void GivenThatThereAreVersionablesThatMatchTheQuery()
        {
            var versionable1 = new Versionable.Index.Model.Versionable { Name = "My model" };

            var versionable2 = new Versionable.Index.Model.Versionable { Name = "My other model" };

            var versionables = new List<Versionable.Index.Model.Versionable>()
            {
                versionable1,
                versionable2
            };

            var dependencies = new List<Versionable.Index.Model.Dependency>()
            {
                new Versionable.Index.Model.Dependency
                {
                    ReferrerId = versionable1.Id,
                    ReferencedId = versionable2.Id,
                     Type = "myType"
                }
            };

            var response = new Response<Versionable.Index.Model.Response>(
                new Versionable.Index.Model.Response()
                {
                    Versionables = versionables,
                    Dependencies = dependencies
                });

            _responseFromMediator = response;

            _mediator
                .Setup(m => m.SendAsync(It.IsAny<Versionable.Index.Model.Query>()))
                .ReturnsAsync(response);
        }

        private void GivenThatThereAreNoVersionablesThatMatchTheQuery()
        {
            var response = new Response<Versionable.Index.Model.Response>(
                new Versionable.Index.Model.Response());

            _responseFromMediator = response;

            _mediator
                .Setup(m => m.SendAsync(It.IsAny<Versionable.Index.Model.Query>()))
                .ReturnsAsync(response);
        }

        private void GivenTheCreateCommandWillSucceed()
        {
            _responseFromMediator = new Response();
            _mediator
                .Setup(m => m.SendAsync(It.IsAny<Versionable.Create.Model.Command>()))
                .ReturnsAsync((Response)_responseFromMediator);
        }

        private void WhenWeGetTheIndexQuery()
        {
            _result = _controller.GetIndexQuery(_indexQuery).Result;
        }

        private void WhenWeGetACreateCommand()
        {
            _result = _controller.GetCreateCommand().Result;
        }

        private void WhenWePostTheCreateCommand()
        {
            _result = _controller.PostCreateCommand(_createCommand).Result;
        }

        private void ThenTheCommandIsSentToTheMediator()
        {
            _mediator.Verify(m => m.SendAsync(_createCommand), Times.Once);
        }

        private void ThenTheHttpStatusCodeIs(HttpStatusCode expectedStatusCode)
        {
            if (_result is StatusCodeResult)
            {
                ((StatusCodeResult)_result).StatusCode.ShouldBe((int)HttpStatusCode.OK);
            }
            else if (_result is ObjectResult)
            {
                ((ObjectResult)_result).StatusCode.ShouldBe((int)HttpStatusCode.OK);
            }
            else
            {
                throw new System.Exception("Unexpected result type");
            }
        }

        private void ThenWeAreReturnedAnEmptyCreateCommand()
        {
            var command = ((ObjectResult)_result).Value as Versionable.Create.Model.Command;
            command.ShouldNotBeNull();
            command.Versionable.ShouldBeNull();
        }

        private void ThenTheVersionablesAreReturned()
        {
            var modelResponse = ((Response<Versionable.Index.Model.Response>)_responseFromMediator).Data;
            ((ObjectResult)_result).Value.ShouldBe(modelResponse);
        }

        private void ThenAnResponseIsReturnedThatContainsNoVersionablesOrDependencies()
        {
            var response = ((ObjectResult)_result).Value as Versionable.Index.Model.Response;
            response.Versionables.Count.ShouldBe(0);
            response.Dependencies.Count.ShouldBe(0);
        }
    }
}
