﻿namespace PackageManager.Host.Tests.Features.Versionable.Index
{
    using System.Globalization;
    using System.Net.Http;
    using BinaryMash.Responses;
    using Flurl;
    using Host.Features;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Shouldly;
    using TestStack.BDDfy;
    using Xunit;

    public class QueryHandlerSpecs : VersionableSpecs
    {
        private readonly Host.Features.Versionable.Index.QueryHandler _queryHandler;

        private Mock<ILogger<Host.Features.Versionable.Index.QueryHandler>> _logger;

        private Host.Features.Versionable.Index.Model.Query _modelQuery;

        private Host.Neo4j.Versionable.Index.Response _neo4jResponse;

        private Host.Features.Versionable.Index.Model.Response _modelResponse;

        private Response<Host.Features.Versionable.Index.Model.Response> _result;

        public QueryHandlerSpecs()
        {
            _logger = new Mock<ILogger<Host.Features.Versionable.Index.QueryHandler>>();
            _queryHandler = new Host.Features.Versionable.Index.QueryHandler(_logger.Object, Mapper.Object, Neo4jOptions);
            _modelQuery = new Host.Features.Versionable.Index.Model.Query();
        }

        [Fact]
        public void ShouldReturnResultsOfVersionableIndexQuery()
        {
            this.Given(_ => _.GivenNeo4jWillReturnAResponse())
                .And(_ => _.GivenThatTheNeo4jResponseCanBeMappedToAModelResponse())
                .When(_ => _.WhenWeQueryTheIndex())
                .Then(_ => _.ThenARequestIsSentToNeo4j())
                .And(_ => _.ThenTheResponseFromNeo4jIsMappedToAResponse())
                .And(_ => _.ThenTheMappedResponseIsReturned())
                .And(_ => _.ThenNoErrorsAreReturned())
                .BDDfy();
        }

        [Fact]
        public void ShouldHandleHttpTimeout()
        {
            this.Given(_ => _.GivenTheHttpRequestToNeo4jWillTimeOut())
                .When(_ => _.WhenWeQueryTheIndex())
                .Then(_ => _.ThenARequestIsSentToNeo4j())
                .And(_ => _.ThenTheResponseContainsAnError())
                .BDDfy();
        }

        [Fact]
        public void ShouldHandleTimeoutFromNeo4j()
        {
            this.Given(_ => _.GivenTheHttpRequestToNeo4jWillThrowAnException())
                .When(_ => _.WhenWeQueryTheIndex())
                .Then(_ => _.ThenARequestIsSentToNeo4j())
                .And(_ => _.ThenTheResponseContainsAnError())
                .BDDfy();
        }

        private void GivenNeo4jWillReturnAResponse()
        {
            _neo4jResponse = new Host.Neo4j.Versionable.Index.Response();
            HttpTest.RespondWithJson(_neo4jResponse);
        }

        private void GivenTheHttpRequestToNeo4jWillTimeOut()
        {
            HttpTest.SimulateTimeout();
        }

        private void GivenTheHttpRequestToNeo4jWillThrowAnException()
        {
            HttpTest.RespondWith(status: 500);
        }

        private void GivenThatTheNeo4jResponseCanBeMappedToAModelResponse()
        {
            _modelResponse = new Host.Features.Versionable.Index.Model.Response();

            Mapper.Setup(m => m.Map<Host.Features.Versionable.Index.Model.Response>(It.IsAny<object>()))
                .Returns(_modelResponse);
        }

        private void WhenWeQueryTheIndex()
        {
            _result = _queryHandler.Handle(_modelQuery).Result;
        }

        private void ThenARequestIsSentToNeo4j()
        {
            var neo4jOptions = Neo4jOptions.Value;

            HttpTest.ShouldHaveCalled(neo4jOptions.BaseUrl.AppendPathSegment(neo4jOptions.CommitTransactionsEndpoint))
                .WithVerb(HttpMethod.Post)
                .WithBasicAuth(neo4jOptions.Username, neo4jOptions.Password)
                .WithContentType("application/json")
                .WithRequestJson(new Host.Neo4j.Versionable.Index.Request());
        }

        private void ThenTheResponseFromNeo4jIsMappedToAResponse()
        {
            // TODO: comparison
            Mapper.Verify(m => m.Map<Host.Features.Versionable.Index.Model.Response>(It.IsAny<Host.Neo4j.Versionable.Index.Response>()), Times.Once);
        }

        private void ThenTheMappedResponseIsReturned()
        {
            _result.Data.ShouldBe(_modelResponse);
        }

        private void ThenNoErrorsAreReturned()
        {
            _result.Errors.ShouldBeEmpty();
        }

        private void ThenTheResponseContainsAnError()
        {
            _result.Errors.Count.ShouldBe(1);
            _result.Errors[0].Code.ShouldBe(Logging.ErrorWhenGettingVersionableIndex.ToString(CultureInfo.InvariantCulture));
            _result.Errors[0].Message.ShouldBe("An error occurred when getting the versionable index");
        }
    }
}