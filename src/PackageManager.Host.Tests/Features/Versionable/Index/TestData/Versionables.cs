﻿namespace PackageManager.Host.Tests.Features.Versionable.Index.TestData
{
    public static class Versionables
    {
        public static Host.Features.Versionable.Index.Model.Versionable Versionable0
        {
            get
            {
                return new Host.Features.Versionable.Index.Model.Versionable()
                {
                    Id = 0,
                    Name = "BinaryMash.PackageManager",
                    Version = "0.0.1"
                };
            }
        }

        public static Host.Features.Versionable.Index.Model.Versionable Versionable1
        {
            get
            {
                return new Host.Features.Versionable.Index.Model.Versionable()
                {
                    Id = 1,
                    Name = "BinaryMash.PackageManager.Host",
                    Version = "0.0.1"
                };
            }
        }

        public static Host.Features.Versionable.Index.Model.Versionable Versionable2
        {
            get
            {
                return new Host.Features.Versionable.Index.Model.Versionable()
                {
                    Id = 2,
                    Name = "BinaryMash.PackageManager.Application",
                    Version = "0.0.1"
                };
            }
        }

        public static Host.Features.Versionable.Index.Model.Versionable Versionable3
        {
            get
            {
                return new Host.Features.Versionable.Index.Model.Versionable()
                {
                    Id = 3,
                    Name = "Flurl",
                    Version = "2.1.0"
                };
            }
        }

        public static Host.Features.Versionable.Index.Model.Versionable Versionable4
        {
            get
            {
                return new Host.Features.Versionable.Index.Model.Versionable()
                {
                    Id = 4,
                    Name = "This is my versionable!",
                    Version = "1.2.3.4"
                };
            }
        }

        public static Host.Features.Versionable.Index.Model.Versionable Versionable5
        {
            get
            {
                return new Host.Features.Versionable.Index.Model.Versionable()
                {
                    Id = 5,
                    Name = "This is my dependent",
                    Version = "2.3.4.5"
                };
            }
        }
    }
}
