﻿namespace PackageManager.Host.Tests.Features.Versionable.Index.TestData
{
    public static class Dependencies
    {
        public static Host.Features.Versionable.Index.Model.Dependency Dependency0
        {
            get
            {
                return new Host.Features.Versionable.Index.Model.Dependency()
                {
                    ReferrerId = Versionables.Versionable0.Id,
                    ReferencedId = Versionables.Versionable1.Id,
                    Type = "nuget"
                };
            }
        }

        public static Host.Features.Versionable.Index.Model.Dependency Dependency1
        {
            get
            {
                return new Host.Features.Versionable.Index.Model.Dependency()
                {
                    ReferrerId = Versionables.Versionable0.Id,
                    ReferencedId = Versionables.Versionable2.Id,
                    Type = "nuget"
                };
            }
        }

        public static Host.Features.Versionable.Index.Model.Dependency Dependency2
        {
            get
            {
                return new Host.Features.Versionable.Index.Model.Dependency()
                {
                    ReferrerId = Versionables.Versionable2.Id,
                    ReferencedId = Versionables.Versionable3.Id,
                    Type = "nuget"
                };
            }
        }

        public static Host.Features.Versionable.Index.Model.Dependency Dependency3
        {
            get
            {
                return new Host.Features.Versionable.Index.Model.Dependency()
                {
                    ReferrerId = Versionables.Versionable4.Id,
                    ReferencedId = Versionables.Versionable5.Id,
                    Type = "nuget"
                };
            }
        }
    }
}
