﻿namespace PackageManager.Host.Tests.Features.Versionable.Index.TestData
{
    public static class Responses
    {
        public static Host.Features.Versionable.Index.Model.Response Nominal
        {
            get
            {
                var response = new Host.Features.Versionable.Index.Model.Response();

                response.Versionables.AddRange(new[]
                {
                    Versionables.Versionable0,
                    Versionables.Versionable1,
                    Versionables.Versionable2,
                    Versionables.Versionable3,
                    Versionables.Versionable4,
                    Versionables.Versionable5
                });

                response.Dependencies.AddRange(new[]
                {
                    Dependencies.Dependency1,
                    Dependencies.Dependency0,
                    Dependencies.Dependency2,
                    Dependencies.Dependency3
                });

                return response;
            }
        }
    }
}
