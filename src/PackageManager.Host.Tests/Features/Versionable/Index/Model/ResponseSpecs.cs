﻿namespace PackageManager.Host.Tests.Features.Versionable.Index.Model
{
    using PackageManager.Host.Features.Versionable.Index.Model;
    using Shouldly;
    using TestStack.BDDfy;
    using Xunit;

    public class ResponseSpecs
    {
        private Response _response;

        [Fact]
        public void ShouldInitialiseEmptyResponse()
        {
            this.When(_ => _.WhenConstructedWithDefaultConstructor())
                .Then(_ => _.ThenThereIsAnEmptyCollectionOfDependencies())
                .And(_ => _.ThenThereIsAnEmptyCollectionOfVersionables())
                .BDDfy();
        }

        private void WhenConstructedWithDefaultConstructor()
        {
            _response = new Response();
        }

        private void ThenThereIsAnEmptyCollectionOfDependencies()
        {
            _response.Dependencies.ShouldBeEmpty();
        }

        private void ThenThereIsAnEmptyCollectionOfVersionables()
        {
            _response.Versionables.ShouldBeEmpty();
        }
    }
}
