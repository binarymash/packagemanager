﻿namespace PackageManager.Host.Tests.Features.Versionable.Create.TestData
{
    using System.Collections.Generic;
    using Model = PackageManager.Host.Features.Versionable.Create.Model;

    public static class Versionables
    {
        public static Model.Versionable Versionable1
        {
            get
            {
                return new Model.Versionable
                {
                    Name = "ThisIsVersionable1",
                    Version = "1.2.3.4",
                    Dependencies = new List<Model.Dependency>
                    {
                        new Model.Dependency
                        {
                            Type = "nuget",
                            Versionable = Versionable2
                        },
                        new Model.Dependency()
                        {
                            Type = "nuget",
                            Versionable = Versionable4
                        }
                    }
                };
            }
        }

        public static Model.Versionable Versionable2
        {
            get
            {
                return new Model.Versionable
                {
                    Name = "ThisIsVersionable2",
                    Version = "2.3.4.5",
                    Dependencies = new List<Model.Dependency>
                    {
                        new Model.Dependency()
                        {
                            Type = "nuget",
                            Versionable = Versionable3
                        }
                    }
                };
            }
        }

        public static Model.Versionable Versionable3
        {
            get
            {
                return new Model.Versionable
                {
                    Name = "ThisIsVersionable3",
                    Version = "3.4.5.6",
                    Dependencies = new List<Model.Dependency>()
                };
            }
        }

        public static Model.Versionable Versionable4
        {
            get
            {
                return new Model.Versionable()
                {
                    Name = "ThisIsVersionable4",
                    Version = "1.1.1.1",
                    Dependencies = new List<Model.Dependency>()
                };
            }
        }
    }
}
