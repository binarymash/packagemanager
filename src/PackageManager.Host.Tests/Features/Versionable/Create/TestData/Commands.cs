﻿namespace PackageManager.Host.Tests.Features.Versionable.Create.TestData
{
    using Model = PackageManager.Host.Features.Versionable.Create.Model;

    public static class Commands
    {
        public static Model.Command NominalValid => new Model.Command
        {
            Versionable = Versionables.Versionable1
        };
    }
}
