﻿namespace PackageManager.Host.Tests.Features.Versionable.Create
{
    using FluentValidation.TestHelper;
    using PackageManager.Host.Features.Versionable.Create.Validation;
    using Utilities;
    using Xunit;

    public class VersionableValidatorSpecs
    {
        private VersionableValidator _validator;

        public VersionableValidatorSpecs()
        {
            _validator = new VersionableValidator();
        }

        [Fact]
        public void ShouldAcceptNameIfBetween3And512Characters()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, TextGenerator.Generate(3) as string);
            _validator.ShouldNotHaveValidationErrorFor(c => c.Name, TextGenerator.Generate(512) as string);
        }

        [Fact]
        public void ShouldRejectNameIfLessThan3OrMoreThan512Characters()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, TextGenerator.Generate(0) as string);
            _validator.ShouldHaveValidationErrorFor(c => c.Name, TextGenerator.Generate(1) as string);
            _validator.ShouldHaveValidationErrorFor(c => c.Name, TextGenerator.Generate(2) as string);
            _validator.ShouldHaveValidationErrorFor(c => c.Name, TextGenerator.Generate(513) as string);
        }

        [Fact]
        public void ShouldRejectNameIfWhitespace()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, "      " as string);
        }

        [Fact]
        public void ShouldRejectNameIfNull()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Name, null as string);
        }
    }
}
