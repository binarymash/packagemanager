﻿namespace PackageManager.Host.Tests.Features.Versionable.Create
{
    using FluentValidation.TestHelper;
    using PackageManager.Host.Features.Versionable.Create.Validation;
    using Utilities;
    using Xunit;

    public class CommandValidatorSpecs
    {
        private CommandValidator _validator;

        public CommandValidatorSpecs()
        {
            _validator = new CommandValidator();
        }

        [Fact]
        public void ShouldAcceptIfContainsAVersionable()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Versionable, new Host.Features.Versionable.Create.Model.Versionable());
        }

        [Fact]
        public void ShouldRejectIfDoesNotContainAVersionable()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Versionable, (Host.Features.Versionable.Create.Model.Versionable)null);
        }
    }
}
