﻿namespace PackageManager.Host.Tests.Features.Versionable.Create
{
    using FluentValidation.TestHelper;
    using PackageManager.Host.Features.Versionable.Create.Validation;
    using Utilities;
    using Xunit;

    public class DependencyValidatorSpecs
    {
        private DependencyValidator _validator;

        public DependencyValidatorSpecs()
        {
            _validator = new DependencyValidator();
        }

        [Fact]
        public void ShouldAcceptTypeIfBetween1And512Characters()
        {
            _validator.ShouldNotHaveValidationErrorFor(d => d.Type, TextGenerator.Generate(1) as string);
            _validator.ShouldNotHaveValidationErrorFor(d => d.Type, TextGenerator.Generate(512) as string);
        }

        [Fact]
        public void ShouldRejectTypeIfIsNull()
        {
            _validator.ShouldHaveValidationErrorFor(d => d.Type, (string)null);
        }

        [Fact]
        public void ShouldRejectTypeIfLessThan3OrMoreThan512Characters()
        {
            _validator.ShouldHaveValidationErrorFor(d => d.Type, TextGenerator.Generate(0) as string);
            _validator.ShouldHaveValidationErrorFor(d => d.Type, TextGenerator.Generate(513) as string);
        }

        [Fact]
        public void ShouldAcceptVersionableIfIsNotNull()
        {
            _validator.ShouldNotHaveValidationErrorFor(c => c.Versionable, new Host.Features.Versionable.Create.Model.Versionable());
        }

        [Fact]
        public void ShouldRejectVersionableIfIsNull()
        {
            _validator.ShouldHaveValidationErrorFor(c => c.Versionable, (Host.Features.Versionable.Create.Model.Versionable)null);
        }
    }
}
