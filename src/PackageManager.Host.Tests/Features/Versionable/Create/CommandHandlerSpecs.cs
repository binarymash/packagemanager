﻿namespace PackageManager.Host.Tests.Features.Versionable.Create
{
    using System.Globalization;
    using BinaryMash.Responses;
    using Flurl;
    using Host.Features;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Newtonsoft.Json;
    using Shouldly;
    using TestStack.BDDfy;
    using Xunit;
    using Model = Host.Features.Versionable.Create.Model;

    public class CommandHandlerSpecs : VersionableSpecs
    {
        private readonly Host.Features.Versionable.Create.CommandHandler _handler;

        private Mock<ILogger<Host.Features.Versionable.Create.CommandHandler>> _logger;

        private Model.Command _command;

        private Host.Neo4j.Versionable.Create.Request _commandMappedToNeo4jRequest;

        private Host.Neo4j.Versionable.Create.Response _responseFromNeo4j;

        private Response _result;

        public CommandHandlerSpecs()
        {
            _logger = new Mock<ILogger<Host.Features.Versionable.Create.CommandHandler>>();
            _handler = new Host.Features.Versionable.Create.CommandHandler(_logger.Object, Mapper.Object, Neo4jOptions);
        }

        [Fact]
        public void ShouldCreateAValidVersionable()
        {
            this.Given(_ => _.GivenAValidCommand())
                .And(_ => _.GivenTheCommandCanBeMappedToANeo4jRequestToCreateVersionables())
                .And(_ => _.GivenThatNeo4jWillAcceptOurRequestToCreateVersionables())
                .When(_ => _.WhenTheCommandIsHandled())
                .Then(_ => _.ThenTheCommandIsMappedToANeo4jRequestToCreateVersionables())
                .And(_ => _.ThenTheRequestIsPostedToNeo4j())
                .And(_ => _.ThenTheResultHasNoErrors())
                .BDDfy();
        }

        [Fact]
        public void ShouldHandleHttpTimeout()
        {
            this.Given(_ => _.GivenTheHttpRequestToNeo4jWillTimeOut())
                .When(_ => _.WhenTheCommandIsHandled())
                .Then(_ => _.ThenTheCommandIsMappedToANeo4jRequestToCreateVersionables())
                .And(_ => _.ThenTheRequestIsPostedToNeo4j())
                .And(_ => _.ThenTheResponseContainsAnError())
                .BDDfy();
        }

        [Fact]
        public void ShouldHandleTimeoutFromNeo4j()
        {
            this.Given(_ => _.GivenTheHttpRequestToNeo4jWillThrowAnException())
                .When(_ => _.WhenTheCommandIsHandled())
                .Then(_ => _.ThenTheCommandIsMappedToANeo4jRequestToCreateVersionables())
                .And(_ => _.ThenTheRequestIsPostedToNeo4j())
                .And(_ => _.ThenTheResponseContainsAnError())
                .BDDfy();
        }

        private void GivenAValidCommand()
        {
            _command = TestData.Commands.NominalValid;
        }

        private void GivenTheCommandCanBeMappedToANeo4jRequestToCreateVersionables()
        {
            _commandMappedToNeo4jRequest = Neo4j.TestData.Versionable.Create.Requests.Nominal;

            Mapper
                .Setup(m => m.Map<Host.Neo4j.Versionable.Create.Request>(It.IsAny<Model.Command>()))
                .Returns(_commandMappedToNeo4jRequest);
        }

        private void GivenThatNeo4jWillAcceptOurRequestToCreateVersionables()
        {
            _responseFromNeo4j = Neo4j.TestData.Versionable.Create.Responses.Ok;
            HttpTest.RespondWithJson(_responseFromNeo4j, 200);
        }

        private void GivenTheHttpRequestToNeo4jWillTimeOut()
        {
            HttpTest.SimulateTimeout();
        }

        private void GivenTheHttpRequestToNeo4jWillThrowAnException()
        {
            HttpTest.RespondWith(status: 500);
        }

        private void WhenTheCommandIsHandled()
        {
            _result = _handler.Handle(_command).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private void ThenTheCommandIsMappedToANeo4jRequestToCreateVersionables()
        {
            Mapper.Verify(m => m.Map<Host.Neo4j.Versionable.Create.Request>(_command));
        }

        private void ThenTheRequestIsPostedToNeo4j()
        {
            var expectedUrl = Neo4jOptions.Value.BaseUrl.AppendPathSegment(Neo4jOptions.Value.CommitTransactionsEndpoint);

            HttpTest.CallLog.Count.ShouldBe(1);
            HttpTest.CallLog[0].Url.ShouldBe(expectedUrl);
            HttpTest.CallLog[0].RequestBody.ShouldBe(JsonConvert.SerializeObject(_commandMappedToNeo4jRequest));
        }

        private void ThenTheResultHasNoErrors()
        {
            _result.Errors.ShouldBeEmpty();
        }

        private void ThenTheResponseContainsAnError()
        {
            _result.Errors.Count.ShouldBe(1);
            _result.Errors[0].Code.ShouldBe(Logging.ErrorWhenCreatingVersionable.ToString(CultureInfo.InvariantCulture));
            _result.Errors[0].Message.ShouldBe("An error occurred when creating a versionable");
        }
    }
}
