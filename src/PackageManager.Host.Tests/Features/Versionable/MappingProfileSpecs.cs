﻿namespace PackageManager.Host.Tests.Features.Versionable
{
    using System.Linq;
    using Shouldly;
    using TestStack.BDDfy;
    using Xunit;
    using Neo4j = Host.Neo4j;
    using Neo4jTestData = Neo4j.TestData;
    using Versionable = Host.Features.Versionable;

    public class MappingProfileSpecs : Features.MappingProfileSpecs<Versionable.MappingProfile>
    {
        private Versionable.Create.Model.Command _modelCreateCommand;

        private Versionable.Index.Model.Response _modelIndexResponse;

        private Neo4j.Versionable.Create.Request _neo4jCreateRequest;

        private Neo4j.Versionable.Index.Response _neo4jIndexResponse;

        [Fact]
        public void ShouldResolveAllMappings()
        {
            this.When(_ => _.WhenAMapperIsCreated())
                .Then(_ => _.ThenAllMappingsAreValid())
                .BDDfy();
        }

        [Fact]
        public void ShouldMapAVersionableCreateCommandToNeo4jRequest()
        {
            this.Given(_ => _.GivenTheMappingProfile())
                .And(_ => _.GivenAVersionableCreateCommand())
                .When(_ => _.WhenAMapperIsCreated())
                .And(_ => _.WhenTheCommandIsMappedToANeo4jRequest())
                .Then(_ => _.ThenTheRequestContainsStatementsForEachDependency())
                .BDDfy();
        }

        [Fact]
        public void ShouldMapANeo4jIndexResponseThatContainsNoResultsToAModelIndexResponse()
        {
            this.Given(_ => _.GivenTheMappingProfile())
                .And(_ => _.GivenANeo4jIndexResponseThatContainsNoResults())
                .When(_ => _.WhenAMapperIsCreated())
                .And(_ => _.WhenTheNeo4jIndexResponseIsMappedToAModelIndexResponse())
                .Then(_ => _.ThenTheResponseContainsNoVersionablesOrDependencies())
                .BDDfy();
        }

        [Fact]
        public void ShouldMapANominalNeo4jIndexResponseToANominalModelIndexResponse()
        {
            this.Given(_ => _.GivenTheMappingProfile())
                .And(_ => _.GivenANominalNeo4jIndexResponse())
                .When(_ => _.WhenAMapperIsCreated())
                .And(_ => _.WhenTheNeo4jIndexResponseIsMappedToAModelIndexResponse())
                .Then(_ => _.ThenWeAreReturnedANominalModelIndexResponse())
                .BDDfy();
        }

        private void GivenAVersionableCreateCommand()
        {
            _modelCreateCommand = Create.TestData.Commands.NominalValid;
        }

        private void GivenANeo4jIndexResponseThatContainsNoResults()
        {
            _neo4jIndexResponse = new Neo4j.Versionable.Index.Response();
        }

        private void GivenANominalNeo4jIndexResponse()
        {
            _neo4jIndexResponse = Neo4jTestData.Versionable.Index.Responses.Nominal;
        }

        private void WhenAMapperIsCreated()
        {
            Mapper = Configuration.CreateMapper();
        }

        private void WhenTheCommandIsMappedToANeo4jRequest()
        {
            _neo4jCreateRequest = Mapper.Map<Neo4j.Versionable.Create.Request>(_modelCreateCommand);
        }

        private void WhenTheNeo4jIndexResponseIsMappedToAModelIndexResponse()
        {
            _modelIndexResponse = Mapper.Map<Versionable.Index.Model.Response>(_neo4jIndexResponse);
        }

        private void ThenAllMappingsAreValid()
        {
            Mapper.ConfigurationProvider.AssertConfigurationIsValid();
        }

        private void ThenTheRequestContainsStatementsForEachDependency()
        {
            ShouldContainStatementsForAllDependenciesOf(_modelCreateCommand.Versionable);
        }

        private void ThenTheResponseContainsNoVersionablesOrDependencies()
        {
            _modelIndexResponse.Versionables.Count.ShouldBe(0);
            _modelIndexResponse.Dependencies.Count.ShouldBe(0);
        }

        private void ThenWeAreReturnedANominalModelIndexResponse()
        {
            var expectedResponse = Index.TestData.Responses.Nominal;

            _modelIndexResponse.Dependencies.Count.ShouldBe(expectedResponse.Dependencies.Count);

            foreach (var expectedDependency in expectedResponse.Dependencies)
            {
                _modelIndexResponse.Dependencies.ShouldContain(d =>
                d.ReferencedId == expectedDependency.ReferencedId &&
                d.ReferrerId == expectedDependency.ReferrerId &&
                d.Type == expectedDependency.Type);
            }

            _modelIndexResponse.Versionables.Count.ShouldBe(expectedResponse.Versionables.Count);

            foreach (var expectedVersionable in expectedResponse.Versionables)
            {
                _modelIndexResponse.Versionables.ShouldContain(v =>
                v.Id == expectedVersionable.Id &&
                v.Name == expectedVersionable.Name &&
                v.Version == expectedVersionable.Version);
            }
        }

        private void ShouldContainStatementsForAllDependenciesOf(Versionable.Create.Model.Versionable versionable)
        {
            foreach (var dependency in versionable.Dependencies)
            {
                _neo4jCreateRequest.Statements.Any(s => ForVersionableAndDependency(s, versionable, dependency)).ShouldBeTrue();
                ShouldContainStatementsForAllDependenciesOf(dependency.Versionable);
            }
        }

        private bool ForVersionableAndDependency(Neo4j.Generic.RequestStatement s, Versionable.Create.Model.Versionable referrer, Versionable.Create.Model.Dependency dependency)
        {
            var statement = s as Neo4j.Versionable.Create.RequestStatement;

            return statement.Statement ==
                $"MERGE (ref:Versionable{{version:'{referrer.Version}', name:'{referrer.Name}'}}) MERGE (dep:Versionable{{version:'{dependency.Versionable.Version}', name:'{dependency.Versionable.Name}'}}) MERGE (ref)-[:REFERENCES{{type: '{dependency.Type}'}}]->(dep) RETURN ref, dep";
        }
    }
}
