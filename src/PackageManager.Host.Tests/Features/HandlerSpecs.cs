﻿namespace PackageManager.Host.Tests.Features
{
    using System;
    using AutoMapper;
    using Flurl.Http.Testing;
    using Microsoft.Extensions.Options;
    using Moq;

    public class HandlerSpecs : IDisposable
    {
        public HandlerSpecs()
        {
            Mapper = new Mock<IMapper>();
            HttpTest = new HttpTest();
            Neo4jOptions = Options.Create(new Host.Neo4j.Neo4jOptions
            {
                BaseUrl = "http://someapi.com",
                CommitTransactionsEndpoint = "/commitTransactionsEndpoint",
                Username = "MyUsername",
                Password = "MyPassword"
            });
        }

        protected IOptions<Host.Neo4j.Neo4jOptions> Neo4jOptions { get; private set; }

        protected MapperConfiguration Configuration { get; private set; }

        protected Mock<IMapper> Mapper { get; private set; }

        protected HttpTest HttpTest { get; private set; }

        public void Dispose()
        {
            HttpTest.Dispose();
        }
    }
}
