﻿namespace PackageManager.Host.Tests.Features
{
    using AutoMapper;

    public class MappingProfileSpecs<TProfile>
        where TProfile : Profile, new()
    {
        public MappingProfileSpecs()
        {
            GivenTheMappingProfile();
        }

        protected MapperConfiguration Configuration { get; private set; }

        protected IMapper Mapper { get; set; }

        protected void GivenTheMappingProfile()
        {
            Configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<TProfile>();
            });

            Mapper = Configuration.CreateMapper();
        }
    }
}
