﻿namespace PackageManager.Host.Tests.Utilities
{
    using System.Text;

    public static class TextGenerator
    {
        public static string Generate(int length)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                sb.Append("a");
            }

            return sb.ToString();
        }
    }
}
