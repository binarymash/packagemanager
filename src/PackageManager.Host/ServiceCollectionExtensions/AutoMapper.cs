﻿namespace PackageManager.Host.ServiceCollectionExtensions
{
    using System.Collections.Generic;
    using global::AutoMapper;
    using Microsoft.Extensions.DependencyInjection;

    public static class AutoMapper
    {
        public static void AddAutoMapper(this IServiceCollection services)
        {
            ////var profiles =
            ////    from t in typeof(AutoMapper).Assembly.GetTypes()
            ////    where typeof(Profile).IsAssignableFrom(t)
            ////    select (Profile)Activator.CreateInstance(t);

            var profiles = new List<Profile>
            {
                new Features.Versionable.MappingProfile()
            };

            var config = new MapperConfiguration(cfg =>
            {
                foreach (var profile in profiles)
                {
                    cfg.AddProfile(profile);
                }
            });

            services.AddSingleton<MapperConfiguration>(config);
            services.AddTransient<IMapper>(sp => sp.GetService<MapperConfiguration>().CreateMapper());
        }
    }
}