﻿namespace PackageManager.Host.ServiceCollectionExtensions
{
    using System.Linq;
    using System.Reflection;
    using MediatR;
    using Microsoft.Extensions.DependencyInjection;

    // See http://stevejgordon.co.uk/cqrs-using-mediatr-asp-net-core
    public static class Mediator
    {
        public static void AddMediator(this IServiceCollection services)
        {
            // Mediatr
            services.AddScoped<IMediator, MediatR.Mediator>();
            services.AddTransient<SingleInstanceFactory>(sp => t => sp.GetService(t));
            services.AddTransient<MultiInstanceFactory>(sp => t => sp.GetServices(t));
            services.AddMediatorHandlers(typeof(Startup).GetTypeInfo().Assembly);
        }

        public static IServiceCollection AddMediatorHandlers(this IServiceCollection services, Assembly assembly)
        {
            var classTypes = assembly.ExportedTypes
                .Select(t => t.GetTypeInfo())
                .Where(IsConcreteClass);

            foreach (var type in classTypes)
            {
                var interfaces = type.ImplementedInterfaces
                    .Select(i => i.GetTypeInfo());

                foreach (var handlerType in interfaces.Where(IsRequestHandler))
                {
                    services.AddTransient(handlerType.AsType(), type.AsType());
                }

                foreach (var handlerType in interfaces.Where(IsAsyncRequestHandler))
                {
                    services.AddTransient(handlerType.AsType(), type.AsType());
                }
            }

            return services;
        }

        public static bool IsConcreteClass(TypeInfo typeInfo)
        {
            return typeInfo.IsClass && !typeInfo.IsAbstract;
        }

        public static bool IsRequestHandler(TypeInfo typeInfo)
        {
            return
                typeInfo.IsGenericType &&
                typeInfo.GetGenericTypeDefinition() == typeof(IRequestHandler<,>);
        }

        public static bool IsAsyncRequestHandler(TypeInfo typeInfo)
        {
            return
                typeInfo.IsGenericType &&
                typeInfo.GetGenericTypeDefinition() == typeof(IAsyncRequestHandler<,>);
        }
    }
}