﻿namespace PackageManager.Host.Features
{
    using System.Collections.Generic;
    using Microsoft.Extensions.Logging;

    public static class Logging
    {
        public const int CreateVersionable = 1000;

        public const int ErrorWhenCreatingVersionable = 1001;

        public const int GetVersionableIndex = 1002;

        public const int ErrorWhenGettingVersionableIndex = 1003;

        private static Dictionary<int, EventId> _eventIDs = new Dictionary<int, EventId>
        {
            { CreateVersionable, new EventId(CreateVersionable, "CreateVersionable") },
            { ErrorWhenCreatingVersionable, new EventId(ErrorWhenCreatingVersionable, "ErrorWhenCreatingVersionable") },
            { GetVersionableIndex, new EventId(GetVersionableIndex, "GetVersionableIndex") },
            { ErrorWhenGettingVersionableIndex, new EventId(ErrorWhenGettingVersionableIndex, "ErrorWhenGettingVersionableIndex") }
        };

        public static Dictionary<int, EventId> EventIDs
        {
            get { return _eventIDs; }
        }
    }
}
