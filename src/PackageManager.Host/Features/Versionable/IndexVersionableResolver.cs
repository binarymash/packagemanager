﻿namespace PackageManager.Host.Features.Versionable
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Index.Model;

    public class IndexVersionableResolver : IValueResolver<Neo4j.Versionable.Index.Response, Index.Model.Response, List<Index.Model.Versionable>>
    {
        public List<Versionable> Resolve(Neo4j.Versionable.Index.Response source, Index.Model.Response destination, List<Versionable> destMember, ResolutionContext context)
        {
            var versionables = new Dictionary<int, Index.Model.Versionable>();

            foreach (var result in source.Results)
            {
                foreach (dynamic datum in result.Data)
                {
                    var id = datum.Meta[0].Id;
                    if (!versionables.ContainsKey(id))
                    {
                        var versionable = new Versionable()
                        {
                            Id = id,
                            Name = datum.Row[0].name,
                            Version = datum.Row[0].version
                        };
                        versionables.Add(versionable.Id, versionable);
                    }

                    id = datum.Meta[2].Id;
                    if (!versionables.ContainsKey(id))
                    {
                        var versionable = new Versionable()
                        {
                            Id = id,
                            Name = datum.Row[2].name,
                            Version = datum.Row[2].version
                        };
                        versionables.Add(versionable.Id, versionable);
                    }
                }
            }

            return versionables.Values.ToList();
        }
    }
}
