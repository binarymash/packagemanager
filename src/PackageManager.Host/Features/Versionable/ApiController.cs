﻿namespace PackageManager.Host.Features.Versionable
{
    using System.Threading.Tasks;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;

    public class ApiController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly IMediator _mediator;

        public ApiController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("api/versionables")]
        public async Task<IActionResult> GetIndexQuery(Index.Model.Query query)
        {
            var mediatorResponse = await _mediator.SendAsync(query);
            return Ok(mediatorResponse.Data);
        }

        [HttpGet]
        [Route("api/versionables/create")]
        public async Task<IActionResult> GetCreateCommand()
        {
            var command = await Task.FromResult(new Create.Model.Command());
            return Ok(command);
        }

        [HttpPost]
        [Route("api/versionables/create")]
        public async Task<IActionResult> PostCreateCommand([FromBody] Create.Model.Command command)
        {
            if (ModelState.IsValid)
            {
                var mediatorResponse = await _mediator.SendAsync(command);

                return Ok();
            }

            return BadRequest(ModelState);
        }
    }
}
