﻿namespace PackageManager.Host.Features.Versionable
{
    using System.Collections.Generic;
    using AutoMapper;

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Create.Model.Command, Neo4j.Versionable.Create.Request>()
                .ForMember(cvdr => cvdr.Statements, opt => opt.MapFrom(c => c.Versionable));

            CreateMap<Create.Model.Versionable, List<Neo4j.Generic.RequestStatement>>()
                .ConvertUsing<VersionableConverter>();

            CreateMap<Neo4j.Versionable.Index.Response, Index.Model.Response>()
                .ForMember(r => r.Versionables, opt => opt.ResolveUsing<IndexVersionableResolver>())
                .ForMember(r => r.Dependencies, opt => opt.ResolveUsing<IndexDependencyResolver>());
        }
    }
}
