﻿namespace PackageManager.Host.Features.Versionable.Create.Validation
{
    using FluentValidation;

    public class CommandValidator : AbstractValidator<Model.Command>
    {
        public CommandValidator()
        {
            RuleFor(m => m.Versionable)
                .NotNull();
        }
    }
}
