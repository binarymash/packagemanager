﻿namespace PackageManager.Host.Features.Versionable.Create.Validation
{
    using FluentValidation;

    public class DependencyValidator : AbstractValidator<Model.Dependency>
    {
        public DependencyValidator()
        {
            RuleFor(m => m.Type)
                .NotNull()
                .NotEmpty()
                .Length(1, 512);

            RuleFor(m => m.Versionable)
                .NotNull();
        }
    }
}
