﻿namespace PackageManager.Host.Features.Versionable.Create.Validation
{
    using FluentValidation;

    public class VersionableValidator : AbstractValidator<Model.Versionable>
    {
        public VersionableValidator()
        {
            RuleFor(m => m.Name)
                .NotNull()
                .NotEmpty()
                .Length(3, 512);
        }
    }
}
