﻿namespace PackageManager.Host.Features.Versionable.Create
{
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using BinaryMash.Responses;
    using Flurl;
    using Flurl.Http;
    using MediatR;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Neo4j;

    public class CommandHandler : IAsyncRequestHandler<Model.Command, Response>
    {
        private readonly ILogger<CommandHandler> _logger;

        private readonly IMapper _mapper;

        private readonly Neo4jOptions _neo4jOptions;

        public CommandHandler(ILogger<CommandHandler> logger, IMapper mapper, IOptions<Neo4jOptions> neo4jOptions)
        {
            _logger = logger;
            _mapper = mapper;
            _neo4jOptions = neo4jOptions.Value;
        }

        public async Task<Response> Handle(Model.Command modelCommand)
        {
            var neo4jRequest = _mapper.Map<Neo4j.Versionable.Create.Request>(modelCommand);

            try
            {
                var neo4jResponse = await _neo4jOptions.BaseUrl
                    .AppendPathSegment(_neo4jOptions.CommitTransactionsEndpoint)
                    .WithBasicAuth(_neo4jOptions.Username, _neo4jOptions.Password)
                    .WithHeader("Accept", "application/json")
                    .WithHeader("X-stream", "true")
                    .PostJsonAsync(neo4jRequest)
                    .ReceiveJson<Neo4j.Versionable.Create.Response>();

                if (!neo4jResponse.Errors.Any())
                {
                    return new Response();
                }

                _logger.LogWarning(Logging.EventIDs[Logging.ErrorWhenCreatingVersionable], "Response from Neo4j contained errors", neo4jResponse.Errors);
            }
            catch (FlurlHttpTimeoutException ex)
            {
                _logger.LogWarning(Logging.EventIDs[Logging.ErrorWhenCreatingVersionable], "The Neo4j request timed out", ex);
            }
            catch (FlurlHttpException ex)
            {
                _logger.LogWarning(Logging.EventIDs[Logging.ErrorWhenCreatingVersionable], "The Neo4j request threw an exception", ex);
            }

            return new Response(new System.Collections.Generic.List<Error>
            {
                new Error(Logging.ErrorWhenCreatingVersionable, "An error occurred when creating a versionable")
            });
        }
    }
}
