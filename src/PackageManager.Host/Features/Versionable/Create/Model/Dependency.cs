﻿namespace PackageManager.Host.Features.Versionable.Create.Model
{
    public class Dependency
    {
        public string Type { get; set; }

        public Versionable Versionable { get; set; }
    }
}
