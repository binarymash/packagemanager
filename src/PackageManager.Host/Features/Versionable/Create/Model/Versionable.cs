﻿namespace PackageManager.Host.Features.Versionable.Create.Model
{
    using System.Collections.Generic;

    public class Versionable
    {
        public string Name { get; set; }

        public string Version { get; set; }

        public List<Dependency> Dependencies { get; set; }
    }
}
