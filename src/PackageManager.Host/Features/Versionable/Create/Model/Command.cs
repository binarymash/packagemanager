﻿namespace PackageManager.Host.Features.Versionable.Create.Model
{
    using BinaryMash.Responses;
    using MediatR;

    public class Command : IAsyncRequest<Response>
    {
        public Versionable Versionable { get; set; }
    }
}