﻿namespace PackageManager.Host.Features.Versionable
{
    using System.Collections.Generic;
    using AutoMapper;

    public class VersionableConverter : ITypeConverter<Create.Model.Versionable, List<Neo4j.Generic.RequestStatement>>
    {
        public List<Neo4j.Generic.RequestStatement> Convert(Create.Model.Versionable source, List<Neo4j.Generic.RequestStatement> destination, ResolutionContext context)
        {
            var statements = new List<Neo4j.Generic.RequestStatement>();

            foreach (var dependency in source.Dependencies)
            {
                statements.Add(new Neo4j.Versionable.Create.RequestStatement(source.Name, source.Version, dependency.Type, dependency.Versionable.Name, dependency.Versionable.Version));
                statements.AddRange(Convert(dependency.Versionable, destination, context));
            }

            return statements;
        }
    }
}
