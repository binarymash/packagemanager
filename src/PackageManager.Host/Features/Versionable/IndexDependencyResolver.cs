﻿namespace PackageManager.Host.Features.Versionable
{
    using System.Collections.Generic;
    using AutoMapper;
    using Index.Model;

    public class IndexDependencyResolver : IValueResolver<Neo4j.Versionable.Index.Response, Index.Model.Response, List<Dependency>>
    {
        public List<Dependency> Resolve(Neo4j.Versionable.Index.Response source, Index.Model.Response destination, List<Dependency> destMember, ResolutionContext context)
        {
            var dependencies = new List<Dependency>();

            foreach (var result in source.Results)
            {
                foreach (dynamic datum in result.Data)
                {
                    var dependency = new Dependency()
                    {
                        Type = datum.Row[1].type,
                        ReferrerId = datum.Meta[0].Id,
                        ReferencedId = datum.Meta[2].Id
                    };
                    dependencies.Add(dependency);
                }
            }

            return dependencies;
        }
    }
}
