﻿namespace PackageManager.Host.Features.Versionable.Index.Model
{
    using System.Collections.Generic;

    public class Versionable
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Version { get; set; }
    }
}
