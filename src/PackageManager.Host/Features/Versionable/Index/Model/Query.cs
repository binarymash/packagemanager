﻿namespace PackageManager.Host.Features.Versionable.Index.Model
{
    using BinaryMash.Responses;
    using MediatR;

    public class Query : IAsyncRequest<Response<Model.Response>>
    {
    }
}
