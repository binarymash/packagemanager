﻿namespace PackageManager.Host.Features.Versionable.Index.Model
{
    public class Dependency
    {
        public string Type { get; set; }

        public int ReferrerId { get; set; }

        public int ReferencedId { get; set; }
    }
}
