﻿namespace PackageManager.Host.Features.Versionable.Index.Model
{
    using System.Collections.Generic;

    public class Response
    {
        public Response()
        {
            Versionables = new List<Versionable>();
            Dependencies = new List<Dependency>();
        }

        public List<Versionable> Versionables { get; set; }

        public List<Dependency> Dependencies { get; set; }
    }
}
