﻿namespace PackageManager.Host.Features.Versionable.Index
{
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using BinaryMash.Responses;
    using Flurl;
    using Flurl.Http;
    using MediatR;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using PackageManager.Host.Neo4j;

    public class QueryHandler : IAsyncRequestHandler<Model.Query, Response<Model.Response>>
    {
        private ILogger<QueryHandler> _logger;

        private IMapper _mapper;

        private Neo4jOptions _neo4jOptions;

        public QueryHandler(ILogger<QueryHandler> logger, IMapper mapper, IOptions<Neo4jOptions> neo4jOptions)
        {
            _logger = logger;
            _neo4jOptions = neo4jOptions.Value;
            _mapper = mapper;
        }

        public async Task<Response<Model.Response>> Handle(Model.Query modelQuery)
        {
            var neo4jRequest = new Neo4j.Versionable.Index.Request();

            try
            {
                var neo4jResponse = await _neo4jOptions.BaseUrl
                    .AppendPathSegment(_neo4jOptions.CommitTransactionsEndpoint)
                    .WithBasicAuth(_neo4jOptions.Username, _neo4jOptions.Password)
                    .WithHeader("Accept", "application/json")
                    .WithHeader("X-stream", "true")
                    .PostJsonAsync(neo4jRequest)
                    .ReceiveJson<Neo4j.Versionable.Index.Response>();

                if (!neo4jResponse.Errors.Any())
                {
                    var modelResponse = _mapper.Map<Model.Response>(neo4jResponse);
                    return new Response<Model.Response>(modelResponse);
                }

                _logger.LogWarning(Logging.EventIDs[Logging.ErrorWhenGettingVersionableIndex], "The Neo4j response contained errors", neo4jResponse.Errors);
            }
            catch (FlurlHttpTimeoutException ex)
            {
                _logger.LogWarning(Logging.EventIDs[Logging.ErrorWhenGettingVersionableIndex], "The Neo4j request timed out", ex);
            }
            catch (FlurlHttpException ex)
            {
                _logger.LogWarning(Logging.EventIDs[Logging.ErrorWhenGettingVersionableIndex], "The Neo4j request threw an exception", ex);
            }

            return new Response<Model.Response>(new System.Collections.Generic.List<Error>
            {
                new Error(Logging.ErrorWhenGettingVersionableIndex, "An error occurred when getting the versionable index")
            });
        }
    }
}
