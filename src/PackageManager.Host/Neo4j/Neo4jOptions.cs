﻿namespace PackageManager.Host.Neo4j
{
    public class Neo4jOptions
    {
        public string BaseUrl { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string CommitTransactionsEndpoint { get; set; }
    }
}
