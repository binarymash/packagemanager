﻿namespace PackageManager.Host.Neo4j.Versionable.Create
{
    public class RequestStatement : Generic.RequestStatement
    {
        private string _referrerName;

        private string _referrerVersion;

        private string _referenceType;

        private string _dependencyName;

        private string _dependencyVersion;

        public RequestStatement(string referrerName, string referrerVersion, string referenceType, string dependencyName, string dependencyVersion)
        {
            _referrerName = referrerName;
            _referrerVersion = referrerVersion;
            _referenceType = referenceType;
            _dependencyName = dependencyName;
            _dependencyVersion = dependencyVersion;
        }

        public override string Statement
        {
            get
            {
                return $"MERGE (ref:Versionable{{version:'{_referrerVersion}', name:'{_referrerName}'}}) MERGE (dep:Versionable{{version:'{_dependencyVersion}', name:'{_dependencyName}'}}) MERGE (ref)-[:REFERENCES{{type: '{_referenceType}'}}]->(dep) RETURN ref, dep";
            }
        }
    }
}
