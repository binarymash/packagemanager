﻿namespace PackageManager.Host.Neo4j.Versionable.Index
{
    public class Request : Generic.Request
    {
        public Request()
        {
            Statements.Add(new RequestStatement());
        }
    }
}
