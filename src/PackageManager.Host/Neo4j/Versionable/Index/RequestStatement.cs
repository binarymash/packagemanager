﻿namespace PackageManager.Host.Neo4j.Versionable.Index
{
    public class RequestStatement : Generic.RequestStatement
    {
        public override string Statement
        {
            get
            {
                return $"optional match (r:Versionable)-[dep:REFERENCES]->(d:Versionable) return r, dep, d";
            }
        }
    }
}
