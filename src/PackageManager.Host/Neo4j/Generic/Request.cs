﻿namespace PackageManager.Host.Neo4j.Generic
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class Request
    {
        public Request()
        {
            Statements = new List<RequestStatement>();
        }

        [JsonProperty("statements")]
        public List<RequestStatement> Statements { get; set; }
    }
}
