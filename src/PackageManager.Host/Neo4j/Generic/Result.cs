﻿namespace PackageManager.Host.Neo4j.Generic
{
    using System.Collections.Generic;

    public class Result
    {
        public Result()
        {
            Columns = new List<string>();
            Data = new List<Datum>();
        }

        public List<string> Columns { get; private set; }

        public List<Datum> Data { get; private set; }
    }
}
