﻿namespace PackageManager.Host.Neo4j.Generic
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public abstract class RequestStatement
    {
        public RequestStatement()
        {
            ResultDataContents = new List<string>()
            {
                "row"
            };
            IncludeStats = false;
        }

        [JsonProperty("statement")]
        public abstract string Statement { get; }

        [JsonProperty("resultDataContents")]
        public List<string> ResultDataContents { get; private set; }

        [JsonProperty("includeStats")]
        public bool IncludeStats { get; private set; }
    }
}
