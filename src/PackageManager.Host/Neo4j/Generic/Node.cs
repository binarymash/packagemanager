﻿namespace PackageManager.Host.Neo4j.Generic
{
    using System.Collections.Generic;
    using System.Dynamic;

    public class Node
    {
        public int Id { get; set; }

        public List<string> Labels { get; set; }

        public ExpandoObject Properties { get; set; }
    }
}
