﻿namespace PackageManager.Host.Neo4j.Generic
{
    using System.Collections.Generic;

    public class Graph
    {
        public List<Node> Nodes { get; set; }

        public List<Relationship> Relationships { get; set; }
    }
}
