﻿namespace PackageManager.Host.Neo4j.Generic
{
    using System;

    public class Transaction
    {
        public DateTime Expires { get; set; }
    }
}
