﻿namespace PackageManager.Host.Neo4j.Generic
{
    using System.Collections.Generic;

    public class Response
    {
        public Response()
        {
            Results = new List<Result>();
            Errors = new List<Error>();
        }

        public List<Result> Results { get; set; }

        public List<Error> Errors { get; set; }
    }
}