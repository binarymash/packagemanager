﻿namespace PackageManager.Host.Neo4j.Generic
{
    using System.Collections.Generic;
    using System.Dynamic;

    public class Datum
    {
        public Datum()
        {
            Row = new List<ExpandoObject>();
            Meta = new List<Meta>();
        }

        public List<ExpandoObject> Row { get; set; }

        public List<Meta> Meta { get; set; }

        public Graph Graph { get; set; }
    }
}
