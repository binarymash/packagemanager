﻿namespace PackageManager.Host.Neo4j.Generic
{
    public class Meta
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public bool Deleted { get; set; }
    }
}
