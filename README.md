[![Build status](https://ci.appveyor.com/api/projects/status/0x74018aqvam3kv6/branch/master?svg=true)](https://ci.appveyor.com/project/binarymash/packagemanager-qdkoj/branch/master)

Running Neo4j in Docker

docker run \
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    neo4j:3.0
	
Point your browser at http://localhost:7474 on Linux or http://$(docker-machine ip default):7474 on OSX.
	